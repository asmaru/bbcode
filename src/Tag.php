<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace asmaru\bbcode;

use Closure;
use function is_callable;
use function preg_quote;

/**
 * Class Tag
 *
 * @package asmaru\bbcode\rule
 */
class Tag implements Rule {

	private ?Closure $callback;

	public function __construct(private readonly string $tag, private readonly string $pattern, ?Closure $callback = null) {
		$this->callback = $callback;
	}

	public function parse(string $input): string {
		return preg_replace_callback('/\[' . preg_quote($this->tag) . '(?:=(?P<value>[^\]]*))?\](?P<text>.*?)\[\/' . preg_quote($this->tag) . '\]/is', function ($matches) {
			$text = $matches['text'];
			$value = !empty($matches['value']) ? $matches['value'] : $text;
			if (is_callable($this->callback)) {
				$value = $this->callback->__invoke($value);
			}
			return sprintf($this->pattern, trim($text), trim($value));
		}, $input);
	}
}