<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace asmaru\bbcode;

use function htmlentities;
use const ENT_QUOTES;

/**
 * Class Parser
 *
 * @package asmaru\bbcode
 */
class Parser {

	/**
	 * @var Rule[]
	 */
	private array $rules = [];

	public function addRule(Rule $rule): void {
		$this->rules[] = $rule;
	}

	public function toHTML(string $input, bool $escapeHTML = true, bool $respectLinebreak = true): string {
		if ($escapeHTML) {
			$input = htmlentities($input, ENT_QUOTES, 'UTF-8');
		}
		foreach ($this->rules as $rule) {
			$input = $rule->parse($input);
		}
		if ($respectLinebreak) {
			$input = str_replace(["\r\n", "\r", "\n"], '<br/>', $input);
		}
		return $input;
	}
}