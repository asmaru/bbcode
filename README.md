# Readme

![Codacy grade](https://img.shields.io/codacy/grade/8c22e50bbb864b00b2c9d5ff178105b5?style=for-the-badge)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/asmaru/bbcode/master?style=for-the-badge)
![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=for-the-badge)

## Usage

```php
$input = <<<'TXT'
[b]BBCode[/b] or [b][i]Bulletin Board Code[/i][/b] is a lightweight markup language used to format posts in many message boards. 
The available tags are usually indicated by square brackets ([ ]) surrounding a keyword, and they are parsed by the message board system 
before being translated into [url=https://en.wikipedia.org/wiki/HTML]HTML[/url].
TXT;

$bbCode = new \asmaru\bbcode\BBCode();
$bbCode->addRule(new \asmaru\bbcode\rule\BoldTag());
$bbCode->addRule(new \asmaru\bbcode\rule\ItalicTag());
$bbCode->addRule(new \asmaru\bbcode\rule\UrlTag());
echo $bbCode->toHTML($input);
```

### Result

```html
<strong>BBCode</strong> or <strong><em>Bulletin Board Code</em></strong> is a lightweight markup language used to format posts in many message boards. 
The available tags are usually indicated by square brackets ([ ]) surrounding a keyword, and they are parsed by the message board system 
before being translated into <a href="https://en.wikipedia.org/wiki/HTML">HTML</a>
```