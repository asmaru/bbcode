<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\bbcode;

use asmaru\bbcode\rule\BlockquoteTag;
use asmaru\bbcode\rule\BoldTag;
use asmaru\bbcode\rule\Heading1Tag;
use asmaru\bbcode\rule\Heading2Tag;
use asmaru\bbcode\rule\Heading3Tag;
use asmaru\bbcode\rule\ImageTag;
use asmaru\bbcode\rule\ItalicTag;
use asmaru\bbcode\rule\ListTag;
use asmaru\bbcode\rule\SpoilerTag;
use asmaru\bbcode\rule\SpotifyTag;
use asmaru\bbcode\rule\SubTag;
use asmaru\bbcode\rule\SupTag;
use asmaru\bbcode\rule\UrlTag;
use asmaru\bbcode\rule\YoutubeTag;
use PHPUnit\Framework\TestCase;
use const false;

class BBCodeTest extends TestCase {

	public function testPatterns() {
		/** @noinspection HtmlUnknownTarget */
		$tests = [
			['<h1>abc</h1>', '[h1]abc[/h1]'],
			['<h2>abc</h2>', '[h2]abc[/h2]'],
			['<h3>abc</h3>', '[h3]abc[/h3]'],
			['<strong>abc</strong>', '[b]abc[/b]'],
			['<em>abc</em>', '[i]abc[/i]'],
			['<blockquote>abc</blockquote>', '[quote]abc[/quote]'],
			['<a href="abc">abc</a>', '[url]abc[/url]'],
			['<a href="def">abc</a>', '[url=def]abc[/url]'],
			['<img alt="" src="def"/>', '[img]def[/img]'],
			['<div class="spoiler">abc</div>', '[spoiler]abc[/spoiler]'],
			['<sub>abc</sub>', '[sub]abc[/sub]'],
			['<sup>abc</sup>', '[sup]abc[/sup]'],
			['<ul><li>a</li><li>b</li><li>c</li></ul>', "[list]*a\n*b\n*c[/list]"],
			['<div class="iframe youtube"><iframe src="https://www.youtube-nocookie.com/embed/123?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>', '[youtube]123[/youtube]'],
			['<div class="iframe spotify"><iframe src="abc" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe></div>', '[spotify]abc[/spotify]']
		];
		$bbCode = new BBCode();
		$bbCode->addRule(new BlockquoteTag());
		$bbCode->addRule(new BoldTag());
		$bbCode->addRule(new Heading1Tag());
		$bbCode->addRule(new Heading2Tag());
		$bbCode->addRule(new Heading3Tag());
		$bbCode->addRule(new ItalicTag());
		$bbCode->addRule(new ListTag());
		$bbCode->addRule(new SpoilerTag());
		$bbCode->addRule(new SpotifyTag());
		$bbCode->addRule(new SubTag());
		$bbCode->addRule(new SupTag());
		$bbCode->addRule(new UrlTag());
		$bbCode->addRule(new YoutubeTag());
		$bbCode->addRule(new ImageTag());
		foreach ($tests as $test) {
			$expected = $test[0];
			$template = $test[1];
			$this->assertEquals($expected, $bbCode->toHTML($template));
		}
	}

	public function testNesting1() {
		$bbCode = new BBCode();
		$bbCode->addRule(new BoldTag());
		$bbCode->addRule(new ItalicTag());
		$expected = '<strong><em>abc</em></strong>';
		$template = '[b][i]abc[/i][/b]';
		$this->assertEquals($expected, $bbCode->toHTML($template));
	}

	public function testNesting2() {
		$bbCode = new BBCode();
		$bbCode->addRule(new BoldTag());
		$expected = '<strong>abc</strong><strong>def</strong>';
		$template = '[b]abc[/b][b]def[/b]';
		$this->assertEquals($expected, $bbCode->toHTML($template));
	}

	public function testHtmlEscape() {
		$bbCode = new BBCode();
		$bbCode->addRule(new BoldTag());
		$expected = '&lt;strong&gt;<strong>&amp;</strong>&lt;/strong&gt;';
		$template = '<strong>[b]&[/b]</strong>';
		$this->assertEquals($expected, $bbCode->toHTML($template));
	}

	public function testCanDisableEscapeHtml() {
		$bbCode = new BBCode();
		$bbCode->setEscapeHTML(false);
		$this->assertEquals('<', $bbCode->toHTML('<'));
	}
}
