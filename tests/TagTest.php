<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace asmaru\bbcode;

use PHPUnit\Framework\TestCase;

class TagTest extends TestCase {

	public function testCanParse() {
		$rule = new Tag('b', '<strong>%s</strong>');
		$input = '[b]abc 123 [] !"§$%&/()=?[/b] [b][b][/b]';
		$expected = '<strong>abc 123 [] !"§$%&/()=?</strong> <strong>[b]</strong>';
		$result = $rule->parse($input);
		$this->assertEquals($expected, $result);
	}

	public function testCanParseWithValue() {
		$rule = new Tag('url', '<a href="%2$s">%1$s</a>');
		$input = '[url=http://www.example.org/]abc 123 [] !"§$%&/()=?[/url]';
		$expected = '<a href="http://www.example.org/">abc 123 [] !"§$%&/()=?</a>';
		$result = $rule->parse($input);
		$this->assertEquals($expected, $result);
	}

	public function testCanParseWithOptionalValue() {
		$rule = new Tag('url', '<a href="%2$s">%1$s</a>');
		$input = '[url]http://www.example.org/[/url]';
		$expected = '<a href="http://www.example.org/">http://www.example.org/</a>';
		$result = $rule->parse($input);
		$this->assertEquals($expected, $result);
	}

	public function testCanParseWithCallback() {
		$rule = new Tag('url', '<a href="%2$s">%1$s</a>', fn(string $value) => 'http://www.example.org/' . $value);
		$input = '[url=abc]123[/url]';
		$expected = '<a href="http://www.example.org/abc">123</a>';
		$result = $rule->parse($input);
		$this->assertEquals($expected, $result);
	}

	public function testCanParseEmpty() {
		$rule = new Tag('b', '<strong>%s</strong>');
		$input = '';
		$expected = '';
		$result = $rule->parse($input);
		$this->assertEquals($expected, $result);
	}
}